"""Class for visualization of attribute metrics"""

__all__ = ["PlotlyVisualization"]
__author__ = ["Ashritha Goramane"]

from enum import Enum
import pandas as pd
import plotly.io as pio

from xpresso.ai.core.data.visualization.plotly import quartile, heatmap, bar, \
    scatter, pie
from xpresso.ai.core.data.exploration.data_type import DataType
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    MultivariatePlotException
from xpresso.ai.core.data.visualization.report import UnivariateReport, \
    MultivariateReport, CombinedReport, ScatterReport, ReportParam
from xpresso.ai.core.commons.utils.constants import REPORT_OUTPUT_PATH, \
    EMPTY_STRING, PLOTLY_ORCA_PORT
from xpresso.ai.core.data.visualization.abstract_visualization import \
    AbstractVisualize, PlotType

# This is put here intentionally. we want plotly to use same port always
pio.orca.config.port = PLOTLY_ORCA_PORT


class PlotlyVisualization(AbstractVisualize):
    """
    Visualization class takes a automl object and provide functions
    to render plots on the metrics of attribute
    Args:
        dataset(:obj StructuredDataset): Structured dataset object on
            which visualization to be performed
    """

    def __init__(self, dataset):
        super().__init__(dataset)
        self.dataset = dataset
        self.attribute_info = dataset.info.attributeInfo
        self.metric = dataset.info.metrics
        self.logger = XprLogger()

    def render_univariate(self, attr_name=None, attr_type=None, plot_type=None,
                          output_format=utils.HTML,
                          output_path=REPORT_OUTPUT_PATH, report=False,
                          report_format=ReportParam.SINGLEPAGE.value,
                          file_name=None, target=None):
        """
        Renders univariate graphs for a particular attribute
        Args:
            attr_name (str): Specific attribute for which plotutils to be
                generated
            attr_type (:obj: `DataType`): Specific type whose plot_type has
            to be changed
            plot_type (:obj: `PlotType`): Specifies the type of plot to be
                generated
            output_format (str): html, png or pdf plots to be generated
            output_path(str): path where the html plots to be stored
            report (bool): if true generates a report
            report_format (:obj: `ReportParam`): whether the attribute
            distribution should be on same page or continued on different pages
            file_name(str): file name of report pdf
            target(str): Name of target variable in dataset
        """
        if target:
            print("Target variable not supported by plotly backend")

        if report_format.lower() not in [ReportParam.SINGLEPAGE.value,
                                         ReportParam.DOUBLEPAGE.value]:
            print("Incorrect report_format provided. Acceptable parameter "
                  "values are [single,double]")

        if report:
            output_format = "png"

        possible_plots = {
            DataType.NUMERIC.value: [PlotType.QUARTILE.value,
                                     PlotType.BAR.value],
            DataType.NOMINAL.value: [PlotType.PIE.value, PlotType.BAR.value],
            DataType.ORDINAL.value: [PlotType.PIE.value, PlotType.BAR.value],
            DataType.DATE.value: [PlotType.PIE.value, PlotType.BAR.value]
        }
        attr_type_plot_mapping = {
            DataType.NUMERIC.value: [PlotType.QUARTILE.value,
                                     PlotType.BAR.value],
            DataType.NOMINAL.value: [PlotType.PIE.value],
            DataType.ORDINAL.value: [PlotType.PIE.value],
            DataType.DATE.value: [PlotType.PIE.value]
        }
        is_plotted = False

        if plot_type is not None and attr_type is not None:
            if plot_type in possible_plots[attr_type]:
                attr_type_plot_mapping[attr_type] = [plot_type]
            else:
                print("{} plot not possible for {} type. "
                      "Hence plotting default.".format(plot_type, attr_type))
        if plot_type is not None and attr_name is not None:
            for attr in self.attribute_info:
                if attr.name == attr_name:
                    break
            if plot_type in possible_plots[attr.type]:
                attr_type_plot_mapping[attr.type] = [plot_type]
            else:
                print("{} plot not possible for {} attribute. "
                      "Hence plotting default.".format(plot_type, attr_name))

        for attr in self.attribute_info:

            if (attr.name != attr_name and attr_name is not None) \
                    or (self.dataset.data[attr.name].isnull().all()):
                continue

            if attr.type == DataType.NUMERIC.value and \
                    PlotType.QUARTILE.value in \
                    attr_type_plot_mapping[attr.type]:
                field = "quartiles"
                is_plotted = self.plot(field, attr, PlotType.QUARTILE.value,
                                       output_format)
            if attr.type == DataType.NUMERIC.value and \
                    PlotType.BAR.value in attr_type_plot_mapping[attr.type]:
                field = "pdf"
                is_plotted = self.plot(field, attr, PlotType.BAR.value,
                                       output_format)

            if (
                    attr.type == DataType.NOMINAL.value or attr.type ==
                    DataType.ORDINAL.value) \
                    and PlotType.PIE.value in attr_type_plot_mapping[attr.type]:
                field = "freq_count"
                is_plotted = self.plot(field, attr, PlotType.PIE.value,
                                       output_format)

            if (
                    attr.type == DataType.NOMINAL.value or attr.type ==
                    DataType.ORDINAL.value) \
                    and PlotType.BAR.value in attr_type_plot_mapping[attr.type]:
                field = "freq_count"
                is_plotted = self.plot(field, attr, PlotType.BAR.value,
                                       output_format)

            if attr.type == DataType.DATE.value and \
                    PlotType.PIE.value in attr_type_plot_mapping[attr.type]:
                plot_key = ["day_count", "month_count", "year_count"]
                for field in plot_key:
                    is_plotted = self.plot(field, attr, PlotType.PIE.value,
                                           output_format)

            if attr.type == DataType.DATE.value and \
                    PlotType.BAR.value in attr_type_plot_mapping[attr.type]:
                plot_key = ["day_count", "month_count", "year_count"]
                for field in plot_key:
                    is_plotted = self.plot(field, attr, PlotType.BAR.value,
                                           output_format)

            if is_plotted is False:
                self.logger.error("Unable to plot for Attribute : ({},{}) "
                                  "with provided plot types parameter: {}"
                                  .format(attr.name, attr.type, plot_type))
        if report:
            UnivariateReport(dataset=self.dataset).create_report(
                output_path=output_path,
                report_format=report_format,
                file_name=file_name)

    def plot(self, field=None, attr=None, plot_type=None, output_format='html'):

        """Renders specific plot for field in metrics depending upon the type
        of plot"""

        if field in attr.metrics.keys():
            self.logger.info("{} plot for Attribute : ({},{},{})".format(
                plot_type, attr.name, attr.type, field))

        else:
            self.logger.info(
                "Attribute : {}, Field : {}. Unable to perform visualization"
                "Key not present in metric".format(attr.name, field))
            return False
        plot_file_name = attr.name + "_" + plot_type
        plot_title = plot_type + ' plot for ' + attr.name
        plot_title = plot_title.capitalize()
        input_1 = list()
        input_2 = list()
        if field == "quartiles":
            axes_labels = self.set_axes_labels(EMPTY_STRING, "values")
            input_1 = list(attr.metrics[field])
        elif field in ["freq_count", "day_count", "month_count", "year_count"]:
            axes_labels = self.set_axes_labels(str(attr.name), "Count")
            input_1 = list(attr.metrics[field].keys())
            input_2 = list(attr.metrics[field].values())
        elif field == "pdf":
            axes_labels = self.set_axes_labels(str(attr.name),
                                               "P({})".format(attr.name))
            input_1 = list(attr.metrics[field].keys())
            input_2 = list(attr.metrics[field].values())
        else:
            return False

        if plot_type == PlotType.QUARTILE.value:
            quartile.new_plot(input_1, output_format=output_format,
                              auto_render=True,
                              plot_title=plot_title, filename=plot_file_name,
                              axes_labels=axes_labels)
            return True
        elif plot_type == PlotType.PIE.value:
            pie.new_plot(input_1, input_2, output_format=output_format,
                         auto_render=True,
                         plot_title=plot_title, filename=plot_file_name)
            return True
        elif plot_type == PlotType.BAR.value:
            bar.new_plot(input_1, input_2, output_format=output_format,
                         auto_render=True,
                         plot_title=plot_title, filename=plot_file_name,
                         axes_labels=axes_labels)
            return True
        return False

    def render_multivariate(self, output_format=utils.HTML,
                            output_path=REPORT_OUTPUT_PATH,
                            report=False, file_name=None):

        """
        Renders multivariate graphs for spearman, pearson, chi-square
        correlation coefficient
        Args:
            output_format (str): html, png or pdf plots to be generated
            output_path(str): path where the html plots to be stored
            report (bool): if true generates a report
            file_name(str): file name of report pdf
        """

        if report:
            output_format = utils.PNG

        correlation = ["pearson", "spearman", "chi_square"]
        is_plotted = False
        for corr in correlation:
            if corr not in self.metric.keys():
                self.logger.info(
                    "Unable to do Multivariate Visualisation for  Field : {}. "
                    "Key not present in metric".format(corr))
                continue
            try:
                self.logger.info("{} plot for Correlation Type : {}".format(
                    PlotType.HEATMAP.value, corr))
                corr_df = pd.Series(list(self.metric[corr].values()),
                                    index=pd.MultiIndex.from_tuples(
                                        self.metric[corr].keys()))

                corr_df = corr_df.unstack()
                input_1 = corr_df.index.values.tolist()
                input_2 = corr_df.columns.values.tolist()
                input_3 = corr_df.to_numpy().tolist()
                plot_file_name = corr
                heatmap.new_plot(input_1, input_2, input_3,
                                 output_format=output_format,
                                 auto_render=True, plot_title=corr,
                                 filename=plot_file_name)
                is_plotted = True

            except MultivariatePlotException as e:
                print("{}".format(e.message))

        if not is_plotted:
            self.logger.error("No key found. Unable to plot for multivariate "
                              "metrics")

        if report:
            self.scatter(output_format=output_format)
            MultivariateReport(self.dataset).create_report(
                output_path=output_path,
                file_name=file_name)

    def scatter(self, attribute_x=None, attribute_y=None,
                output_format=utils.HTML, file_name=None,
                output_path=REPORT_OUTPUT_PATH, report=False):
        """
        Renders scatter plots for the numeric attributes
        Args:
            attribute_x(str): x axis attribute
            attribute_y(str): y axis attribute
            output_format (str): html, png or pdf plots to be generated
            output_path(str): path where the html/pdf/png plots to be stored
            file_name(str): File name of the plot to be stored
            report (bool): if true generates a report
        """
        output_path_report = None
        if report:
            output_format = utils.PNG
            output_path_report = output_path
        if output_format == utils.HTML:
            output_path = utils.DEFAULT_PDF_PATH
        elif output_format == utils.PNG:
            output_path = utils.DEFAULT_IMAGE_PATH
        else:
            return

        numeric_attr = self.dataset.info.metrics["numeric_attributes"]
        if attribute_x is not None and attribute_x not in numeric_attr:
            self.logger.info("{} not in numeric attribute".format(attribute_x))
            return
        if attribute_y is not None and attribute_y not in numeric_attr:
            self.logger.info("{} not in numeric attribute".format(attribute_y))
            return

        if attribute_x is not None and attribute_y is not None:
            axes_labels = self.set_axes_labels(attribute_x, attribute_y)
            plot_file_name = "{}_scatter".format(attribute_x)
            plot_title = "Scatter plot for {} {}".format(attribute_x,
                                                         attribute_y)
            scatter.new_plot(self.dataset.data[attribute_x],
                             self.dataset.data[attribute_y],
                             axes_labels=axes_labels, auto_render=True,
                             output_format=output_format,
                             filename=plot_file_name, output_path=output_path,
                             plot_title=plot_title)
        elif attribute_x is not None:
            plots = []
            plot_file_name = "{}_scatter".format(attribute_x)
            plot_title = "Scatter plot for {}".format(attribute_x)
            for attribute_y in numeric_attr:
                if attribute_x == attribute_y:
                    continue
                axes_labels = self.set_axes_labels(attribute_x, attribute_y)
                plots.append(scatter.new_plot(self.dataset.data[attribute_x],
                                              self.dataset.data[attribute_y],
                                              axes_labels=axes_labels,
                                              auto_process=True))
                if len(plots) != 4:
                    continue
                scatter.join(plots=plots, output_format=output_format,
                             filename=plot_file_name, output_path=output_path,
                             plot_title=plot_title)
                plots = []
            if plots:
                scatter.join(plots=plots, output_format=output_format,
                             filename=plot_file_name, output_path=output_path,
                             plot_title=plot_title)

        elif attribute_x is None and attribute_y is None:
            for attribute_x in numeric_attr:
                plots = []
                plot_file_name = "{}_scatter".format(attribute_x)
                plot_title = "Scatter plot for {}".format(attribute_x)
                for attribute_y in numeric_attr:
                    if attribute_x == attribute_y:
                        continue
                    axes_labels = self.set_axes_labels(attribute_x, attribute_y)
                    plots.append(
                        scatter.new_plot(self.dataset.data[attribute_x],
                                         self.dataset.data[attribute_y],
                                         axes_labels=axes_labels,
                                         auto_process=True))
                    if len(plots) != 4:
                        continue
                    scatter.join(plots=plots, output_format=output_format,
                                 filename=plot_file_name,
                                 output_path=output_path,
                                 plot_title=plot_title)
                    plots = []
                if plots:
                    scatter.join(plots=plots, output_format=output_format,
                                 filename=plot_file_name,
                                 output_path=output_path,
                                 plot_title=plot_title)
        if report:
            ScatterReport(dataset=self.dataset).create_report(
                output_path=output_path_report,
                file_name=file_name)

    def render_all(self, output_format=utils.HTML,
                   output_path=REPORT_OUTPUT_PATH, report=False,
                   report_format=ReportParam.SINGLEPAGE.value, file_name=None,
                   target=None):
        """
        Renders all univariate and multivariate graphs
        Args:
            output_format (str): html, png or pdf plots to be generated
            output_path(str): path where the html plots or report to be stored
            report (bool): if true generates a report
            report_format (:obj: `ReportParam`): whether the attribute
            distribution should be on same page or continued on different pages
            file_name(str): file name of report pdf
            target(str): Name of target variable in dataset
        """
        if target:
            print("Target variable not supported by plotly backend")

        if report: output_format = utils.PNG

        for attr in self.attribute_info:
            self.render_univariate(attr_name=attr.name,
                                   output_format=output_format)
        self.render_multivariate(output_format=output_format)

        self.scatter(output_format=output_format)

        if report:
            CombinedReport(dataset=self.dataset).create_report(
                output_path=output_path, file_name=file_name,
                report_format=report_format)
